# 004 - JoV

Resources for **van der Linden, L.**, Mathôt, S., Vitu, F. (2015). The role of object affordances and center of gravity in eye movements towards isolated daily-life objects. *Journal of Vision. 15(5)*, 1-18. [doi:10.1167/15.5.8](http://jov.arvojournals.org/article.aspx?articleid=2276779)

## Data

Raw EDF files:

- `/exp1/data`
- `/exp2/data`

## Stimuli

- `/exp1/stimuli`
- `/exp2/stimuli`

## Experimental scripts:

- `exp1/exp1.opensesame.tar.gz`
- `exp2/exp2.opensesame.tar.gz`

## License and credits

- Experimental scripts are available under the [GNU General Public License v3](http://www.gnu.org/licenses/gpl.html).
- The stimuli are taken from:
	- the [Ecological alternative to Snodgrass and Vanderwart](http://dx.plos.org/10.1371/journal.pone.0037527 ), available under a [Creative Commons Attribution](https://creativecommons.org/licenses/by/3.0/)
	- the [Bank of standardized stimuli (BOSS)](http://sites.google.com/site/mathieubrodeur/Home/boss), available under a [Creative Commons Attribution-ShareAlike License](https://creativecommons.org/licenses/by-sa/3.0/) 
- The non-objects are generated with the [Portilla & Simoncelli texture-synthesis algorithm (2000)](http://www.cns.nyu.edu/lcv/texture/)